#!/bin/bash

# RELEASE can be set to any of https://debian.koha-community.org/koha/dists/
if [[ -z "${RELEASE}" ]]; then
    echo "NO RELEASE ENVIRONMENT VARIABLE SET"
    echo "RELEASE can be set to any of https://debian.koha-community.org/koha/dists/"
    exit 1;
else
    echo "RELEASE: $RELEASE"
fi

apt update
apt install -y wget gnupg
wget -O- http://debian.koha-community.org/koha/gpg.asc | apt-key add -

if [ "$RELEASE" == "master" ]; then
    echo "deb http://debian.koha-community.org/koha-staging dev main" >> /etc/apt/sources.list.d/koha.list
else
    echo "deb http://debian.koha-community.org/koha $RELEASE main buster" > /etc/apt/sources.list.d/koha.list
fi

apt update
apt install -y koha-perldeps bash-completion docbook-xsl-ns
